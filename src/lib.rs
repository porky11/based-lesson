use std::{
    fmt::Display,
    fs::File,
    io::{Read, Write},
    path::Path,
    thread,
    time::Duration,
};

use radix_fmt::radix;
use rand::rngs::ThreadRng;
use rustyline::{Editor, error::ReadlineError};

pub trait LessonContext: Sized + Display {
    const COUNT: u32;
    const MINIMUM_BASE: u8 = 1;
    type Checked;

    fn phase(count: u32) -> u32 {
        count
    }

    fn create(rng: &mut ThreadRng, base: u8, level: u32, phase: u32) -> (Self, String);

    fn parse(&self, entered: &str) -> Option<Self::Checked>;
    fn check(&self, checked: Self::Checked) -> bool;
}

pub fn run<C: LessonContext>(config_file: impl AsRef<Path>) {
    let mut rng = rand::thread_rng();

    let mut total = 0;
    let mut correct = 0;

    let mut args = std::env::args();

    args.next();
    let mut base = args.next().map_or(10, |s| {
        u8::from_str_radix(&s, 36).expect("Invalid base specified!") + 1
    });

    let mut point_bytes = [0; 144];

    let cache_path = dirs::cache_dir().map(|mut dir| {
        dir.push(config_file);
        dir
    });

    if let Some(path) = &cache_path {
        if let Ok(mut file) = File::open(path) {
            let _ = file.read_exact(&mut point_bytes);
        }
    }

    if base == 1 {
        let mut prime_factor_values = [0; 36];
        for num in 2..=36 {
            let add = if num == 3 { 2 } else { 1 };
            for i in 1.. {
                let i = i * num;
                if i > 36 {
                    break;
                }
                prime_factor_values[i - 1] += add;
            }
        }

        let mut base_level = None;
        for new_base in C::MINIMUM_BASE..36 {
            let start = new_base as usize * 4;

            let bytes = [
                point_bytes[start],
                point_bytes[start + 1],
                point_bytes[start + 2],
                point_bytes[start + 3],
            ];
            let level = (1 + u32::from_le_bytes(bytes)) * new_base as u32
                / prime_factor_values[new_base as usize];

            if let Some(base_level) = base_level {
                if level >= base_level {
                    continue;
                }
            };

            base_level = Some(level);
            base = new_base + 1;
        }
    }

    println!("Base: {} + 1", radix(base - 1, base));
    println!();

    let (mut points, repeat) = if let Some(s) = args.next() {
        (
            (u32::from_str_radix(&s, base as u32).expect("Invalid base specified!") - 1) * C::COUNT,
            false,
        )
    } else {
        let start = (base - 1) as usize * 4;

        let bytes = [
            point_bytes[start],
            point_bytes[start + 1],
            point_bytes[start + 2],
            point_bytes[start + 3],
        ];
        let points = u32::from_le_bytes(bytes);
        (points / 2, points % 2 == 1)
    };

    let start_points = points;

    let hundred = base as u32;
    let hundred = hundred * hundred;

    let mut exp: i32 = 0;
    let mut streak = 0;
    let mut last_correct = false;

    let mut line_reader = Editor::<()>::new().expect("Line reader could not be created!");

    'outer: loop {
        let phase = C::phase(points % C::COUNT);
        let level = 1 + points / C::COUNT;

        println!("Level {}-{}:", radix(level, base), radix(1 + phase, base));

        let (context, prompt) = C::create(&mut rng, base, level, phase);

        loop {
            let entered = line_reader.readline(&prompt);
            let entered = match entered {
                Ok(line) => {
                    line_reader.add_history_entry(line.as_str());
                    line
                }
                Err(ReadlineError::Interrupted) => continue,
                Err(ReadlineError::Eof) => break 'outer,
                Err(_) => {
                    println!("Input error!");
                    continue;
                }
            };

            let Some(entered) = context.parse(entered.trim()) else {
                println!("Invalid input!");
                continue;
            };

            let next_correct = context.check(entered);

            if last_correct != next_correct {
                streak = 0;
                last_correct = next_correct;
            }

            break;
        }

        streak += 1;

        let required = base as i32 * level as i32;

        if last_correct {
            correct += 1;
            exp += (streak as f32).sqrt() as i32;
            while exp >= required {
                exp -= required;
                points += 1;
                if points % C::COUNT == 0 {
                    break;
                }
            }
            print!("Correct!");
        } else {
            exp -= ((streak * base as i32) as f32).sqrt() as i32;
            while points > 0 && exp <= -required {
                exp += required;
                points -= 1;
                if points % C::COUNT == C::COUNT - 1 {
                    break;
                }
            }
            print!("Wrong!");
        }
        if streak > 1 {
            println!(" (×{})", radix(streak, base));
        } else {
            println!();
        }
        thread::sleep(Duration::from_millis(400));
        if !last_correct {
            println!("Solution: {context}");
            thread::sleep(Duration::from_millis(800));
        }

        total += 1;
        let ratio = correct * hundred / total;
        println!(
            "{} / {} ({}%)",
            radix(correct, base),
            radix(total, base),
            radix(ratio, base),
        );
        println!();
    }

    if let Some(path) = cache_path {
        let mut file = File::create(path).expect("Saving failed");

        let mut points = (start_points * 2 + points).saturating_sub(start_points);
        if repeat {
            points += 1;
        }
        let single_point_bytes = points.to_le_bytes();
        let start = (base - 1) as usize * 4;

        point_bytes[start] = single_point_bytes[0];
        point_bytes[start + 1] = single_point_bytes[1];
        point_bytes[start + 2] = single_point_bytes[2];
        point_bytes[start + 3] = single_point_bytes[3];

        let _ = file.write_all(&point_bytes);
    }
}
